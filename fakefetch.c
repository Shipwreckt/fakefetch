#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//    __      _         __     _       _
//   / _|    | |       / _|   | |     | |
//  | |_ __ _| | _____| |_ ___| |_ ___| |__
//  |  _/ _` | |/ / _ \  _/ _ \ __/ __| '_ \
//  | || (_| |   <  __/ ||  __/ || (__| | | |
//  |_| \__,_|_|\_\___|_| \___|\__\___|_| |_|

// Note: I have tried to make this as user friendly as possable/

//----------------------------------------------------------------------------------|
//                               Welcome to fakefetch                               |
//----------------------------------------------------------------------------------|
//                             This is just neofetch but                            |
//                             You do it yourself, now in C!                        |
//----------------------------------------------------------------------------------|
//             This is Libre code, you can do whatever you want with it since       |
//                                  it is under the GPL                             |
// ---------------------------------------------------------------------------------|
// Function to execute shell commands and get the output                            |
// ---------------------------------------------------------------------------------|
char* execute_command(const char* command) {
    char* result = malloc(128 * sizeof(char));
    FILE* fp;
    if ((fp = popen(command, "r")) == NULL) {
        printf("Error opening pipe!\n");
        return NULL;
    }
    if (fgets(result, 128, fp) == NULL) {
        printf("Error reading pipe!\n");
        return NULL;
    }
    pclose(fp);

    // Remove newline character
    result[strcspn(result, "\n")] = '\0';
    return result;
}

int main() {
    // ---------------------------------------------------------------------------------------------------//
    // Colour codes
    // ---------------------------------------------------------------------------------------------------//
    
    // Define colours by changing the last two letters to Xterm number
    const char* PURPLE = "\033[38;5;92m";
    const char* MAGENTA = "\033[38;5;98m";
    const char* RESET = "\033[0m";
    
    // Set colours
    const char* Text = MAGENTA;
    const char* Background = PURPLE;

    // ---------------------------------------------------------------------------------------------------//
    
    // Get system information
    char* USERNAME = execute_command("whoami");
    char* HOSTNAME = execute_command("uname -n");
    char* DISTRO = execute_command("lsb_release -sd | sed 's/\"//g'");
    char* CPU_INFO = execute_command("lscpu | grep 'Model name' | awk '{$1=$2=\"\"; print $0}' | sed 's/^[ \\t]*//'");
    char* CORES = execute_command("lscpu | grep 'Core(s) per socket' | awk '{print $NF}'");
    char* GPU_INFO = execute_command("lspci | grep -i 'VGA\\|3D' | sed 's/.*: //; s/ (.*//'");
    char* UPTIME = execute_command("uptime -p | sed 's/up //'");

    // Terminal and package manager
    const char* TERMINAL = "Alacritty";                              //Still need to work on the terminal bit 
    char* PACKAGE_MANAGER = execute_command(
        "if command -v pacman &> /dev/null; then echo Pacman; "
        "elif command -v apt-get &> /dev/null; then echo APT; "
        "else echo Unknown; fi"
    );
    
    // ---------------------------------------------------------------------------------------------------//
    // Print output
    // ---------------------------------------------------------------------------------------------------//
    printf("%s                   ::                %s\n", Background, RESET);
    printf("%s                  .==.               %s\n", Background, RESET);
    printf("%s                  ====               %s\n", Background, RESET);
    printf("%s                 -=====              %s@%s%s\n", Background, USERNAME, HOSTNAME, RESET);
    printf("%s                -======              ========================================%s\n", Background, RESET);
    printf("%s               :========-            OS  → %s%s%s\n", Background, Text, DISTRO, RESET);
    printf("%s              :.:-=======-           GPU → %s%s%s\n", Background, Text, GPU_INFO, RESET);
    printf("%s             :==---=======-          PM  → %s%s%s\n", Background, Text, PACKAGE_MANAGER, RESET);
    printf("%s            :==============-         Term → %s%s%s\n", Background, Text, TERMINAL, RESET);
    printf("%s           :================-        Uptime → %s%s%s\n", Background, Text, UPTIME, RESET);
    printf("%s          :==================-       ========================================%s\n", Background, RESET);
    printf("%s         :========----========-      %s\n", Background, RESET);
    printf("%s        -========.    .========-     %s\n", Background, RESET);
    printf("%s       -========        =========    %s\n", Background, RESET);
    printf("%s      -========:        :======---.  %s\n", Background, RESET);
    printf("%s     -=========:        .=======-:.  %s\n", Background, RESET);
    printf("%s    =========--:        .--=======-: %s\n", Background, RESET);
    printf("%s   .=====-:..                ..:-=====%s\n", Background, RESET);
    printf("%s .==-:.                          .:-==:%s\n", Background, RESET);

    // Free up memory
    free(USERNAME);
    free(HOSTNAME);
    free(DISTRO);
    free(CPU_INFO);
    free(CORES);
    free(GPU_INFO);
    free(UPTIME);
    free(PACKAGE_MANAGER);

    return 0;
}
