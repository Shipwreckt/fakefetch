IMPORTANT!!!!
Modify the fakefetch.c yourself to make sure it is how you want it

Fakefetch is just a little command I made that runs in the terminal, it just tells
the person some things about there system, it won't do anything like reading your 
memory or cpu, it just prints an output into your terminal.

Note: This command does change the colour of your terminal input, I would suggest using 
synth-shell with fakefetch. 

webpage: https://shipwreckt.co.uk/projects/programs/Fakefetch.html#info

If you want to make your own designes I suggest these tools, I use them to make my ascci art:

This is my go too because it is offline and easy to use.
https://github.com/TheZoraiz/ascii-image-converter

This is just a website if you do not want to install a program 
https://www.asciiart.eu/image-to-ascii
